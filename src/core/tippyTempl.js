export const tippyContent = (depends, enables) => {
    return `
        <div class="command_tippy">Requires:</div>
        ${depends.length ? `${depends.map(depend => `${depend}`)}` : 'None'}<br />
        <div class="command_tippy">Enables:</div>
        ${enables.length ? `${enables.map(enable => `${enable}`)}` : 'None'}<br />
    `;
};
