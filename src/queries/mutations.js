import gql from "graphql-tag";

export const INSERT_NEW_COMMANDS = gql`
  mutation execute_command(
    $channelId: String!
    $command: String!
    $userId: String!
    $userName: String!
  ) {
    EXECUTE_COMMAND(
      command: {
        channel_id: $channelId
        command: $command
        user_id: $userId
        user_name: $userName
      }
    ) {
      text
    }
  }
`;

export const PLAYER_ENQUEUE_ON_PURCHASE = gql`
  mutation player_join_attempt(
    $userId: String!
    $totalCommands: Int!
    $channelId: String!
    $expired: Boolean!
  ) {
    insert_PLAYERS(
      objects: {
        user_id: $userId
        total_commands: $totalCommands
        expired: $expired
        players_active_session: {
          data: { channel_id: $channelId, total_commands: $totalCommands }
          on_conflict: {
            constraint: PLAYER_SESSION_pkey
            update_columns: total_commands
          }
        }
      }
      on_conflict: {
        constraint: PLAYERS_pkey
        update_columns: [total_commands, expired]
      }
    ) {
      affected_rows
    }
  }
`;

export const CREATE_NEW_GAME_SESSION = gql`
  mutation create_new_game_session($game: [GAME_SESSION_insert_input!]!) {
    insert_GAME_SESSION(
      objects: $game
      on_conflict: { constraint: GAME_SESSION_pkey, update_columns: active }
    ) {
      affected_rows
    }
  }
`;

export const PLAYER_JOIN_ATTEMPT = gql`
  mutation player_join_attempt(
    $channelId: String!
    $userId: String!
    $attempts: Int!
    $totalCommands: Int!
  ) {
    insert_PLAYER_SESSION(
      objects: {
        channel_id: $channelId
        user_id: $userId
        join_attempts: $attempts
        total_commands: $totalCommands
      }
      on_conflict: {
        constraint: PLAYER_SESSION_pkey
        update_columns: join_attempts
      }
    ) {
      affected_rows
    }
  }
`;

export const UPDATE_PLAYER_STATUS = gql`
  mutation allow_player_to_play(
    $channelId: String!
    $userId: String!
    $status: Boolean!
  ) {
    update_PLAYER_SESSION(
      where: { channel_id: { _eq: $channelId }, user_id: { _eq: $userId } }
      _set: { active: $status }
    ) {
      affected_rows
    }
  }
`;

export const REMOVE_PLAYER_FROM_GAME = gql`
  mutation remove_player_from_game($channelId: String!, $userId: String!) {
    delete_PLAYER_SESSION(
      where: { channel_id: { _eq: $channelId }, user_id: { _eq: $userId } }
    ) {
      affected_rows
    }
  }
`;

export const UPDATE_GAME_STATUS = gql`
  mutation update_game_status($channelId: String!, $status: Boolean!) {
    update_GAME_SESSION(
      where: { channel_id: { _eq: $channelId } }
      _set: { active: $status }
    ) {
      affected_rows
    }
  }
`;
