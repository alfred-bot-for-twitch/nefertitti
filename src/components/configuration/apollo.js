import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { WebSocketLink } from "apollo-link-ws";
import { split } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { getMainDefinition } from "apollo-utilities";

export const createAthenaClient = hasuraToken => {
  return function() {
    const getHeaders = () => {
      const headers = {};
      const token = window.localStorage.getItem("token");
      if (token) {
        headers.authorization = `Bearer ${token}`;
      }
      return headers;
    };

    const cache = new InMemoryCache();

    const wsLink = new WebSocketLink({
      uri: "wss://consul.live/v1/graphql",
      options: {
        reconnect: true,
        lazy: true,
        connectionParams: () => {
          return { headers: getHeaders() };
        }
      }
    });

    const httpLink = new HttpLink({
      uri: "https://consul.live/v1/graphql",
      headers: getHeaders()
    });

    const link = split(
      ({ query }) => {
        const { kind, operation } = getMainDefinition(query);
        return kind === "OperationDefinition" && operation === "subscription";
      },
      wsLink,
      httpLink
    );

    const client = new ApolloClient({
      link,
      cache
    });
    return client;
  };
};
