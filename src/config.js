import Config from './components/configuration/Config.svelte';

const app = new Config({
	target: document.body
});

export default app;