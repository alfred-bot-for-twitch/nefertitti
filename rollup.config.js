import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import replace from 'rollup-plugin-replace';
import autoPreprocess from 'svelte-preprocess';
import strip from 'rollup-plugin-strip';
import css from 'rollup-plugin-css-only';

const production = !process.env.ROLLUP_WATCH;console.log(production);

export default ['main'].map((name, index) => ({
	input: `src/${name}.js`,
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: `public/${name}.js`
	},
	plugins: [
		replace({
			'process.env.NODE_ENV': JSON.stringify( 'production' )
		}),
		strip({
			// set this to `false` if you don't want to
			// remove debugger statements
			debugger: true,
	  
			// defaults to `[ 'console.*', 'assert.*' ]`
			functions: [ 'console.log', 'assert.*', 'debug', 'alert' ],
	  
			// set this to `false` if you're not using sourcemaps –
			// defaults to `true`
			sourceMap: false
		}),
		svelte({
			// enable run-time checks when not in production
			dev: !production,
			// we'll extract any component CSS out into
			// a separate file — better for performance

			preprocess: autoPreprocess({
				postcss: {
					plugins: [require('autoprefixer')({})],
				  },
			}),

			css: css => {
				css.write(`public/${name}.css`);
			}
		}),
		css({
				output: 'public/extra.css'
		}),

		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration —
		// consult the documentation for details:
		// https://github.com/rollup/rollup-plugin-commonjs
		resolve({
			browser: true,
			dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/')
		}),
		commonjs(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		!production && livereload('public'),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser()
	],
	watch: {
		clearScreen: false
	}
}));
