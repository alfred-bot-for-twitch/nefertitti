import gql from "graphql-tag";

export const LIVE_GAME_SESSION = gql`
  subscription game_info($channelId: String!) {
    GAME_SESSION(where: {channel_id: {_eq: $channelId}, active: {_eq: true}}) {
      active
      players_needed
      players_in_game(where: {channel_id: {_eq: $channelId}, active: {_eq: true}}) {
        players_details {
          total_commands
          user_id
          user_name
          players_active_session_aggregate {
            aggregate {
              sum {
                commands_executed
              }
            }
          }
        }
      }
    }
  }
`;

export const PLAYER_INFO = gql`
  subscription player_info($channelId: String!, $userId: String!) {
    PLAYERS(where: {user_id: {_eq: $userId}, expired: {_eq: false}}) {
      players_active_session(where: {channel_id: {_eq: $channelId}}) {
        commands_executed
        active
        join_attempts
      }
      user_name
      total_commands
      expired
      players_active_session_aggregate(where: {active: {_eq: true}}) {
        aggregate {
          sum {
            commands_executed
          }
        }
      }
    }
  }
`;
