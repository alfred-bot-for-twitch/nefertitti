import { writable } from "svelte/store";

export const commandGroupState = writable({
  activeCommandGroup: ""
});

export const playerState = writable(null);

export const isLoading = writable(false);
